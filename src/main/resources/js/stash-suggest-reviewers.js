define('suggested-reviewers', [
    'jquery',
    'underscore',
    'util/events',
    'model/page-state',
    'exports'
], function(
    $,
    _,
    events,
    pageState,
    exports) {

    var popups = {};

    // assumes popup with same id will always have same w/h/header
    function spawnPopup(id, w, h, header) {
        var popup = popups[id];
        if (!popup) {
            popup = popups[id] = new AJS.Dialog({
                width: w,
                height: h,
                id: id,
                closeOnOutsideClick: false
            });
            popup.addHeader(header);
            popup.addCancel("Close", function() {
                popup.hide();
            });
            popup.addPanel("", "", id + "-content");
        }
        popup.show();
        return $("." + id + "-content").empty();
    }

    exports.init = function() {
        events.on("stash.model.page-state.changed.sourceBranch", exports.refreshSuggestedReviewers);
        events.on("stash.model.page-state.changed.targetBranch", exports.refreshSuggestedReviewers);

        // trigger on page load, in case the sourceBranch & targetBranch were set by query params
        exports.refreshSuggestedReviewers();
    };

    exports.refreshSuggestedReviewers = function() {
        var fromRepo = $("input[name=fromRepoId]").val();
        var fromBranch = $("input[name=fromBranch]").val();

        var toRepo = $("input[name=toRepoId]").val();
        var toBranch = $("input[name=toBranch]").val();

        if (fromRepo && fromBranch && toRepo && toBranch) {
            exports.showSuggestedReviewers(fromRepo, fromBranch, toRepo, toBranch);
        }

        $("a.more-suggested-reviewers").die().live("click", function() {
            exports.showMoreSuggestedReviewers(fromRepo, fromBranch, toRepo, toBranch);
        });
    };

    var suggested = {}; // user cache
    var shortReasons = {}; // user cache

    exports.suggestReviewers = function(fromRepo, fromBranch, toRepo, toBranch, count, success, error) {
        $.ajax(AJS.contextPath() + "/rest/suggest-reviewers/1.0/by/ref",
            {
                data: {
                    fromRepoId: fromRepo,
                    from: fromBranch,
                    toRepoId: toRepo,
                    to: toBranch,
                    count: count,
                    avatarSize: 64
                },
                error: function(xhr, status, errorMsg) {
                    error(status + " " + errorMsg);
                },
                success: function(data) {
                    // filter out the current user and anyone already added to the review
                    var filterOut = [pageState.getCurrentUser().name];
                    _.each($("#reviewers").val().split("|!|"), function(alreadyAdded) {
                        filterOut.push(alreadyAdded);
                    });
                    data = _.filter(data, function(suggestion) {
                        return filterOut.indexOf(suggestion.user.name) == -1;
                    });

                    // cache the user data for later lookup
                    _.each(data, function(suggestion) {
                        suggested[suggestion.user.name] = suggestion.user;
                        shortReasons[suggestion.user.name] = suggestion.shortReason;
                    });

                    success(data);
                }
            }
        );
    };

    exports.showSuggestedReviewers = function(fromRepo, fromBranch, toRepo, toBranch) {
        var $content = $("input.pull-request-reviewers + div.description");

        $content.empty().html("<span>Suggestions: </span><div class='description-spinner'></div>")
                        .find(".description-spinner").spin("small");

        exports.suggestReviewers(fromRepo, fromBranch, toRepo, toBranch, 20, function(data) {
            $content.html($(stash.suggest.reviewers.oneLine({
                suggestedReviewers: data.slice(0, Math.min(4, data.length)),
                more: false
                // Disable the "more" view until we make the more dialog prettier
                // more: data.length >= 4
            })));
        }, function(errorMsg) {
            $content.text("Failed to load suggested reviewers: " + errorMsg)
        });
    };

    // adds a reviewer to the select2 form input
    var addReviewer = function() {
        var $this = $(this);
        var username = $this.attr("data-name");
        var $reviewers = $("#reviewers");
        var reviewers = $reviewers.val();
        reviewers = reviewers ? reviewers.split("|!|") : [];

        // only add if not already present
        if (reviewers.indexOf(username) == -1) {
            reviewers.push(username);
            $reviewers.val(reviewers.join("|!|"));

            $(".pull-request-reviewers ul.select2-choices li.select2-search-field").before(
                $(stash.suggest.reviewers.reviewerLozenge({
                    user: suggested[username]
                })
            ));
        }

        if ($this.hasClass("aui-button")) {
            // button on dialog
            $this.closest("tr").fadeOut();
        } else {
            // one-line link
            $this.next("span.comma").remove();
            $this.hide(); // shipit hack to workaround hover bug
        }

        // load more reviewers if we've just added the last one
        if ($(".add-suggested-reviewer:visible").length === 0) {
            $(".tipsy").hide(); // another hack to clear visible tooltips
            exports.refreshSuggestedReviewers();
        }
    };

    exports.showMoreSuggestedReviewers = function(fromRepo, fromBranch, toRepo, toBranch) {
        var $content = spawnPopup("suggested-reviewers", 800, 600, "Suggested Reviewers");
        $content.spin('large');
        exports.suggestReviewers(fromRepo, fromBranch, toRepo, toBranch, 20, function(data) {
            $content.empty().html(stash.suggest.reviewers.dialogContent({
                suggestedReviewers: data
            }));
        }, function(error) {
            $content.empty().text(error);
        });
        $content.on("click", ".detailed-add-suggested-reviewer", addReviewer);
    };

    $("input.pull-request-reviewers + div.description").on("click", ".add-suggested-reviewer", addReviewer);

    $(".pull-request-reviewers").on("click", ".remove-suggested-reviewer", function() {
        var $this = $(this);
        var username = $this.attr("data-name");
        var $reviewers = $("#reviewers");
        var reviewers = _.filter($reviewers.val().split("|!|"), function(reviewer) {
            return reviewer !== username;
        });
        $reviewers.val(reviewers.join("|!|"));
        $this.closest("li.select2-search-choice").remove();
    });

    $(".add-suggested-reviewer").tooltip({
        title: function() {
            var username = $(this).attr("data-name");
            return shortReasons[username];
        },
        live: true
    });

});

AJS.$(document).ready(function($) {
    return function() {
        require("suggested-reviewers").init();
    };
}(AJS.$));