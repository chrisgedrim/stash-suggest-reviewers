package com.atlassian.stash.suggestreviewers.internal.util;

public class StringUtils {

    public static String pluralize(int amount, String single, String plural) {
        return amount == 1 ? single : plural;
    }

}
