package com.atlassian.stash.suggestreviewers.internal.util;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.CommandBuilderSupport;
import com.atlassian.stash.scm.git.GitScmConfig;

public class GitUtil {

    public static void setAlternateIfCrossRepository(CommandBuilderSupport builder, Repository repository,
                                               Repository secondRepository, GitScmConfig config) {
        if (!repository.getId().equals(secondRepository.getId())) {
            builder.environment("GIT_ALTERNATE_OBJECT_DIRECTORIES", config.getObjectsDir(secondRepository).getAbsolutePath());
        }
    }

}
