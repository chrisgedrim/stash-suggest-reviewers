package com.atlassian.stash.suggestreviewers.internal.suggester;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetCallback;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.suggestreviewers.internal.util.IntHolder;
import com.atlassian.stash.suggestreviewers.spi.Reason;
import com.atlassian.stash.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.stash.suggestreviewers.spi.SimpleReason;
import com.atlassian.stash.suggestreviewers.spi.UserResolver;
import com.atlassian.stash.user.StashUser;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import java.io.IOException;
import java.util.Map;

import static com.atlassian.stash.suggestreviewers.internal.util.StringUtils.pluralize;

public class ContributorSuggester implements ReviewerSuggester {

    private static final int SCORE_PER_COMMIT = 1000;
    private static final int MAX_COMMITS = 1000;

    private final HistoryService historyService;
    private final UserResolver userResolver;

    public ContributorSuggester(HistoryService historyService, UserResolver userResolver) {
        this.historyService = historyService;
        this.userResolver = userResolver;
    }

    @Override
    public Multimap<StashUser, Reason> suggestFor(Changeset since, Changeset until) {
        final Map<String, IntHolder> emailAddresses = Maps.newHashMap();

        ChangesetsBetweenRequest req = new ChangesetsBetweenRequest.Builder(until.getRepository())
            .include(until.getId())
            .exclude(since.getId())
            .secondaryRepository(until.getRepository())
            .build();

        final IntHolder commitCounter = new IntHolder();

        historyService.streamChangesetsBetween(req, new ChangesetCallback() {
            @Override
            public boolean onChangeset(Changeset changeset) throws IOException {
                String email = changeset.getAuthor().getEmailAddress();
                IntHolder count = emailAddresses.get(email);
                if (count == null) {
                    count = new IntHolder();
                    emailAddresses.put(email, count);
                }
                count.increment();
                return commitCounter.increment() < MAX_COMMITS;
            }

            @Override
            public void onEnd() throws IOException {
                // noop
            }

            @Override
            public void onStart() throws IOException {
                // noop
            }
        });

        Multimap<StashUser, Reason> suggestions = HashMultimap.create();
        for (Map.Entry<String, IntHolder> entry : emailAddresses.entrySet()) {
            StashUser user = userResolver.resolve(entry.getKey());
            if (user != null) {
                int commitCount = entry.getValue().get();
                String description = commitCounter.increment() < MAX_COMMITS ?
                        "Authored " + commitCount + " " + pluralize(commitCount, "commit", "commits") + " to be merged." :
                        "Authored commits to be merged.";
                suggestions.put(
                    user,
                    new SimpleReason(description, commitCount * SCORE_PER_COMMIT)
                );
            }
        }
        return suggestions;
    }

}
