package com.atlassian.stash.suggestreviewers.internal.suggester;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.suggestreviewers.spi.Reason;
import com.atlassian.stash.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.stash.suggestreviewers.spi.SimpleReason;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;
import com.atlassian.stash.util.PageUtils;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class IndiscriminateSuggester implements ReviewerSuggester {

    private static final int SCORE_PER_USER = 1;

    private final UserService userService;

    public IndiscriminateSuggester(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Multimap<StashUser, Reason> suggestFor(Changeset since, Changeset until) {
        Multimap<StashUser, Reason> suggested = HashMultimap.create();
        for (StashUser user : userService.findUsers(PageUtils.newRequest(0, 25)).getValues()) {
            suggested.put(user, new SimpleReason("Just 'cos.", SCORE_PER_USER));
        }
        return suggested;
    }

}
