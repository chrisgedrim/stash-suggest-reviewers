package com.atlassian.stash.suggestreviewers.internal.util;

import com.atlassian.stash.io.LineReader;
import com.atlassian.stash.io.LineReaderOutputHandler;
import com.atlassian.stash.scm.CommandOutputHandler;

import javax.annotation.Nullable;
import java.io.IOException;

/**
 * Returns the first line of output provided by the git process.
 */
public class FirstLineOutputHandler extends LineReaderOutputHandler implements CommandOutputHandler<String> {

    public FirstLineOutputHandler() {
        super("UTF-8");
    }

    private String sha;

    @Nullable
    @Override
    public String getOutput() {
        return sha;
    }

    @Override
    protected void processReader(LineReader lineReader) throws IOException {
        sha = lineReader.readLine();

        // ignore the rest of the output
        while (lineReader.readLine() != null);
    }

}
