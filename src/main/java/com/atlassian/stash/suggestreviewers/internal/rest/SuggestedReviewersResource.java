package com.atlassian.stash.suggestreviewers.internal.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.exception.NoSuchEntityException;
import com.atlassian.stash.exception.NoSuchRepositoryException;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.i18n.KeyedMessage;
import com.atlassian.stash.project.Project;
import com.atlassian.stash.repository.Ref;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryMetadataService;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.rest.util.BadRequestException;
import com.atlassian.stash.rest.util.ResponseFactory;
import com.atlassian.stash.rest.util.RestUtils;
import com.atlassian.stash.suggestreviewers.SuggestedReviewer;
import com.atlassian.stash.suggestreviewers.SuggestedReviewerService;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.stash.suggestreviewers.internal.rest.RestSuggestedReviewer.TO_REST;

@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Path("by")
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Singleton
public class SuggestedReviewersResource {

    private final SuggestedReviewerService suggestedReviewerService;
    private final HistoryService historyService;
    private final RepositoryMetadataService metadataService;
    private final RepositoryService repositoryService;
    private final I18nService i18nService;

    public SuggestedReviewersResource(SuggestedReviewerService suggestedReviewerService, HistoryService historyService,
                                      RepositoryMetadataService metadataService, RepositoryService repositoryService,
                                      I18nService i18nService) {
        this.suggestedReviewerService = suggestedReviewerService;
        this.historyService = historyService;
        this.metadataService = metadataService;
        this.repositoryService = repositoryService;
        this.i18nService = i18nService;
    }

    @GET
    @Path("sha")
    public Response forShas(@QueryParam("fromRepoId") Integer fromRepoId,
                            @QueryParam("from") String fromSha,
                            @QueryParam("toRepoId") Integer toRepoId,
                            @QueryParam("to") String toSha,
                            @QueryParam("count") @DefaultValue("5") int count) {
        checkParams(fromRepoId, fromSha, toRepoId, toSha);

        // not found exceptions automatically handled by ExceptionMapper
        Changeset toHead = historyService.getChangeset(requireRepository(toRepoId), toSha);
        Changeset fromHead = historyService.getChangeset(requireRepository(fromRepoId), fromSha);

        return suggestReviewers(fromHead, toHead, count);
    }

    @GET
    @Path("ref")
    public Response forRefs(@QueryParam("fromRepoId") Integer fromRepoId,
                            @QueryParam("from") String fromRef,
                            @QueryParam("toRepoId") Integer toRepoId,
                            @QueryParam("to") String toRef,
                            @QueryParam("count") @DefaultValue("5") int count) {
        checkParams(fromRepoId, fromRef, toRepoId, toRef);

        // not found exceptions automatically handled by ExceptionMapper
        Changeset toHead = getLatestChangeset(requireRepository(toRepoId), toRef);
        Changeset fromHead = getLatestChangeset(requireRepository(fromRepoId), fromRef);

        return suggestReviewers(fromHead, toHead, count);
    }

    private void checkParams(Integer fromRepoId, String from, Integer toRepoId, String to) {
        if (fromRepoId == null || from == null || toRepoId == null || to == null) {
            throw new BadRequestException(i18nService.getText("suggest.reviewers.params.required",
                    "The 'fromRepoId', 'from', 'toRepoId' and 'to' parameters are all required."));
        }
    }

    private Repository requireRepository(int fromRepoId) {
        Repository repository = repositoryService.getById(fromRepoId);
        if (repository == null) {
            KeyedMessage message = i18nService.getKeyedText("suggest.reviewer.no.such.repository", "No repository with id {0} exists.", fromRepoId);
            throw new NoSuchRepositoryException(message, (Project) null);
        }
        return repository;
    }

    private Response suggestReviewers(Changeset fromHead, Changeset toHead, int count) {
        Iterable<SuggestedReviewer> reviewers = suggestedReviewerService.getSuggestedReviewers(fromHead, toHead, count);
        Iterable<RestSuggestedReviewer> restReviewers = Lists.newArrayList(Iterables.transform(reviewers, TO_REST));
        return ResponseFactory.ok(restReviewers).build();
    }

    private Changeset getLatestChangeset(Repository repository, String refName) {
        Ref ref = metadataService.resolveRef(repository, refName);
        if (ref == null) {
            throw new NoSuchEntityException(i18nService.getKeyedText("no.such.ref", "Couldn't find ref: {0}", refName));
        }
        return historyService.getChangeset(repository, ref.getLatestChangeset());
    }

}
