package com.atlassian.stash.suggestreviewers.internal.suggester;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.git.GitScm;
import com.atlassian.stash.scm.git.GitScmCommandBuilder;
import com.atlassian.stash.scm.git.GitScmConfig;
import com.atlassian.stash.scm.git.blame.GitBlameBuilder;
import com.atlassian.stash.suggestreviewers.internal.suggester.git.*;
import com.atlassian.stash.suggestreviewers.internal.util.GitUtil;
import com.atlassian.stash.suggestreviewers.spi.Reason;
import com.atlassian.stash.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.stash.suggestreviewers.spi.SimpleReason;
import com.atlassian.stash.suggestreviewers.spi.UserResolver;
import com.atlassian.stash.throttle.ThrottleService;
import com.atlassian.stash.throttle.Ticket;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.util.Pair;
import com.google.common.base.Function;
import com.google.common.collect.*;

import java.util.*;

import static com.atlassian.stash.suggestreviewers.internal.util.StringUtils.pluralize;

public class BlameSuggester implements ReviewerSuggester {

    private static final int MAX_SCORE_PER_FILE = 100;
    private static final int MAX_FILES_CONSIDERED = 5;

    private static final String SCM_COMMAND = "scm-command";

    private final GitScm gitScm;
    private final GitScmConfig config;
    private final ThrottleService throttleService;
    private final UserResolver userResolver;
    private final I18nService i18nService;

    public BlameSuggester(GitScm gitScm, GitScmConfig config, ThrottleService throttleService,
                          UserResolver userResolver, I18nService i18nService) {
        this.gitScm = gitScm;
        this.config = config;
        this.throttleService = throttleService;
        this.userResolver = userResolver;
        this.i18nService = i18nService;
    }

    @Override
    public Multimap<StashUser, Reason> suggestFor(Changeset since, Changeset until) {
        Multimap<StashUser, SimpleReason> suggestions = HashMultimap.create();
        Ticket ticket = throttleService.acquireTicket(SCM_COMMAND);
        try {
            final DiffSummary diffSummary = getDiffSummary(since, until);

            Iterator<Map.Entry<String, Pair<Integer, Integer>>> iterator = getPathDeltas(since, until).iterator();
            for (int i = 0; i < MAX_FILES_CONSIDERED && iterator.hasNext(); i++) {
                Map.Entry<String, Pair<Integer, Integer>> delta = iterator.next();
                String path = delta.getKey();

                if (diffSummary.getCreatedPaths().contains(path)) {
                    // if the path was created by one of the commits being considered, there's no point looking
                    // at historical blame
                    i--;
                    continue;
                }

                Set<Map.Entry<StashUser, Integer>> entries = getBlame(until.getRepository(), since.getRepository(),
                        path, since.getId()).entrySet();

                if (entries.isEmpty()) {
                    // no blame = missing path, shouldn't happen but let's be defensive
                    i--;
                    continue;
                }

                for (Map.Entry<StashUser, Integer> author : entries) {
                    boolean added = delta.getValue().getFirst() > delta.getValue().getSecond();

                    int locDelta = added ? delta.getValue().getFirst() : delta.getValue().getSecond();
                    int authorBlame = author.getValue();

                    if (diffSummary.getDeletedPaths().contains(path)) {
                        suggestions.put(author.getKey(), new SimpleReason(
                                "Previously contributed to files that were deleted.",
                                String.format("Authored %s %s of %s, which was deleted.",
                                        authorBlame, pluralize(authorBlame, "line", "lines"),
                                        lastSegment(path)), authorBlame * locDelta));
                    } else {
                        suggestions.put(
                                author.getKey(), new SimpleReason(
                                "Previously contributed to files that were modified.",
                                String.format("Authored %s %s of %s, which had %s %s %s %s it.",
                                        authorBlame, pluralize(authorBlame, "line", "lines"),
                                        lastSegment(path), locDelta, pluralize(locDelta, "line", "lines"),
                                        added ? "added" : "removed", added ? "to" : "from"),
                                        authorBlame * locDelta));
                    }
                }
            }
        } finally {
            ticket.release();
        }
        return scaleScores(suggestions);
    }

    private Multimap<StashUser, Reason> scaleScores(Multimap<StashUser, SimpleReason> suggestions) {
        if (suggestions.isEmpty()) {
            return HashMultimap.create();
        }

        final int max = Collections.max(suggestions.values()).getScore();

        return Multimaps.transformValues(suggestions, new Function<SimpleReason, Reason>() {
            @Override
            public Reason apply(SimpleReason input) {
                int scaledScore = (int) (MAX_SCORE_PER_FILE * ((float) input.getScore()) / max);
                return new SimpleReason(input.getShortDescription(), input.getDescription(), scaledScore);
            }
        });
    }

    private String lastSegment(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    private Map<StashUser, Integer> getBlame(Repository repository, Repository secondary, String path, String at) {
        Map<StashUser, Integer> blame = Maps.newHashMap();

        GitBlameBuilder builder = gitScm.getCommandBuilderFactory()
                .builder(repository)
                .blame()
                .incremental()
                .file(path)
                .rev(at)
                .exitHandler(new MissingPathIgnoringExitHandler(i18nService));
        GitUtil.setAlternateIfCrossRepository(builder, repository, secondary, config);

        Map<String, Integer> blameByEmail = builder.build(new BlameAttributionOutputHandler()).call();

        if (blameByEmail == null) {
            return blame; // path didn't exist @ since revision, ignore
        }

        for (Map.Entry<String, Integer> entry : blameByEmail.entrySet()) {
            StashUser user = userResolver.resolve(entry.getKey());
            if (user != null) {
                blame.put(user, entry.getValue());
            }
        }
        return blame;
    }

    private DiffSummary getDiffSummary(Changeset since, Changeset until) {
        DiffSummaryOutputHandler outputHandler = new DiffSummaryOutputHandler();
        GitScmCommandBuilder builder = gitScm.getCommandBuilderFactory()
                .builder(since.getRepository())
                .command("diff")
                .argument("--summary")
                .argument(since.getId() + ".." + until.getId());
        GitUtil.setAlternateIfCrossRepository(builder, since.getRepository(), until.getRepository(), config);
        builder.build(outputHandler).call();
        return outputHandler;
    }

    private List<Map.Entry<String, Pair<Integer, Integer>>> getPathDeltas(Changeset since, Changeset until) {
        GitScmCommandBuilder builder = gitScm.getCommandBuilderFactory()
                .builder(since.getRepository())
                .command("diff")
                .argument("--numstat")
                .argument(since.getId() + ".." + until.getId());
        GitUtil.setAlternateIfCrossRepository(builder, since.getRepository(), until.getRepository(), config);
        Map<String, Pair<Integer, Integer>> deltas = builder.build(new DiffNumstatOutputHandler()).call();
        return sortByAbsDelta(deltas);
    }

    private List<Map.Entry<String, Pair<Integer, Integer>>> sortByAbsDelta(Map<String, Pair<Integer, Integer>> pathDeltas) {
        List<Map.Entry<String, Pair<Integer, Integer>>> list = Lists.newArrayList(pathDeltas.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Pair<Integer, Integer>>>() {
            @Override
            public int compare(Map.Entry<String, Pair<Integer, Integer>> a, Map.Entry<String, Pair<Integer, Integer>> b) {
                return Math.max(b.getValue().getFirst(), Math.abs(b.getValue().getSecond())) -
                       Math.max(a.getValue().getFirst(), Math.abs(a.getValue().getSecond()));
            }
        });
        return list;
    }

}
