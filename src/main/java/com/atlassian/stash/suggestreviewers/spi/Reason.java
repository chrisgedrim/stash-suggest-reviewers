package com.atlassian.stash.suggestreviewers.spi;

import com.atlassian.stash.user.StashUser;

/**
 * The reason a particular {@link StashUser} is being suggested as a reviewer.
 *
 * @since 1.0
 */
public interface Reason extends Comparable<Reason> {

    /**
     * @return a short description of the reason (should be less than 50 characters for
     * aesthetic reasons, but this is not enforced).
     */
    String getShortDescription();

    /**
     * <strong>This is not displayed to the user in version 1.0, but may be in a future release.</strong>
     *
     * @return a longer description of the reason (should be less than 256 characters for
     * aesthetic reasons, but this is not enforced).
     */
    String getDescription();

    /**
     * @return a number showing how appropriate the reviewer is. The higher the number, the
     * more appropriate the reviewer.
     */
    int getScore();

}
